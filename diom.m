function [x,relres,resvec] = diom(A,b,tol,maxit,x0, trunc, M1,M2)
% [x,relres,comp_resvec] = diom(A,b,tol,maxit,x0)
% [x,relres,comp_resvec] = diom(A,b,tol,maxit,x0,trunc)
% [x,relres,comp_resvec] = diom(A,b,tol,maxit,x0,trunc, M1)
% [x,relres,comp_resvec] = diom(A,b,tol,maxit,x0,trunc, M1,M2)
% 
% This is an implementation of truncated FOM as described by Saad in his
% book Iterative Methods for Sparse Linear Systems.  It is called DIOM and
% it is on page 163, Algorithm 6.8.  However, this version includes LU
% factorization with partial pivoting, which was described in 
% Practical Use of Some Krylov Subspace Methods for
% Solving Indefinite and Nonsymmetric Linear Systems by Yousef Saad from
% SIAM J. ScI. STAT. COMPUT. Vol. 5, No. 1, March 1984
% 
% VARIABLES
%
% INPUTS:
% A            This is a square matrix of size N
% b            The right hand side of Ax=b, it is an N-vector
% trunc        This tells us how many previous search directions should we
%              orthogonalize against (truncation number)
% tol          The relative residual tolerance
% maxit        Defines a maximum number of iterations to run before exiting regardless of reaching tolerance
% x0           An intial guess solution
% lprecond     An optional left precondtioner.
% 
% OUTPUTS:
% x            The approximation being returned
% relres       The returned relative residual
% comp_resvec  A vector of all computed residuals
% true_resvec  A vector of all true residuals
% P            This is a N x trunc matrix of search directions.  We need
%              the previous trunc-1 directions to compute the next one
%
% INTERMEDIATE VARIABLES:
% V            This is the N x trunc matrix with the last trunc arnoldi vectors as
%              columns
% h            Stores the current column of the upper Hessenberg and is
%              transformed into the current column of U from the LU
%              factorization of the upper Hessenberg.
% l            A vector containing the elements of the first subdiagonal of
%              L from LU = H
% perm         Store the row permutations of LU in vector form.  It starts
%              out as the vector false(maxit,1). If row j and j+1 are exchanged
%              then p(j) = true.  
%
% Copyright (c) 2013 Kirk M. Soodhalter
% Version 2.01

%===============================================================================
% Copyright (c) 2013 Kirk Soodhalter
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction for non-commercial purposes, including
% without limitation the rights to use, copy, modify, merge, publish, and/or
% distribute copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in
% all copies or substantial portions of the Software, and credit has to be
% given to the authors in publications that are in any form based on this
% Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.
%===============================================================================

if ~exist('trunc','var')
    trunc = maxit;
end

if ~exist('M1','var')
    M1 = [];
end

if ~exist('M2','var')
    M2 = [];
end

islprecond = ~isempty(M1);
isrprecond = ~isempty(M2);

N = size(A,1);
V = zeros(N, maxit);
P = zeros(N, maxit);
l = zeros(maxit,1);
resvec = zeros(maxit,1);

perm = false(maxit,1);

x = x0;
r = b - A*x;
t = zeros(size(x));
if islprecond
    r = M1\r;
end
relres = 1;
beta = norm(r);

if beta <= tol
    display('Initial guess has residual below tolerance.');
    return;
end

resvec(1) = beta;
iter = 1;
V(:,iter) = r/beta;
zeta = beta;

while true
    fprintf('.');
    
    %ARNOLDI
    [V(:,iter+1),h] = mgs_arnoldi_step(A, V(:,1:iter), trunc, M1,M2);
        
    %PREV. ROW PERMUTATIONS AND OPERATIONS
    %TRANSFORMS COLUMN H(:,iter) INTO [U(:,iter);H(iter+1,iter)]
    for i = max(2,iter-trunc+1):iter
        if  perm(i-1)
            h(i-1:i) = [h(i);h(i-1)];
        end
        h(i) = h(i) - h(i-1)*l(i);
    end
    
    %COMPUTE RESIDUAL NORM
    resvec(iter+1) = h(iter+1)*abs(zeta/h(iter));
    relres = resvec(iter+1)/beta;
    
    %ROW PERMUTE IF |H(iter+1,iter)| > |H(iter,iter)|
    if abs(h(iter+1)) > abs(h(iter))
        perm(iter) = true;
        h(iter:iter+1) = [h(iter+1);h(iter)];
    end
    l(iter+1) = h(iter+1)/h(iter);
    
    if abs(h(iter)) < 1e-8
        rank_deff_warning = ['At step %d Upper Hessenberg has become near rank deficient resulting in a \n'... 
                             'near zero value in the last diagonal value of U in the factorization P*H = L*U.\n'];
        warning(rank_deff_warning,iter);
    end
    
    %NEW SEARCH DIRECTION
    P(:,iter) = V(:,iter);
    temp_sum = 0;
    for i = max((iter - trunc),1):(iter-1)
        temp_sum = temp_sum + h(i)*P(:,i);
    end
    P(:,iter) = (P(:,iter) - temp_sum)/h(iter);    

    if iter == 1 && ~perm(iter)
        t = t + zeta*P(:,iter);
    else
        if iter ~= 1 && ~perm(iter-1)
            zeta = -l(iter)*zeta;
        end
        
        if ~perm(iter) || relres < tol
            t = t + zeta*P(:,iter);
        end
    end
    
    %ARE WE BELOW TOLERANCE?
    if relres <= tol
        % Calculate final solution and residual.
        if(isrprecond)
           x = x0 + M2 \ t;
        else
           x = x0 + t;
        end
        %IS ACTUAL RESIDUAL BELOW TOLERANCE?
        if norm(b - A*x) <= tol*beta
            display(sprintf('\n DIOM Converged at step %d with relative residual %0.5g',iter,relres));
            break; 
        end
    end
    
    %HAVE WE REACHED MAXIMUM ITERATIONS WITHOUT CONVERGENCE?
    if iter == maxit
        display(sprintf('\n DIOM reached maximum iterations without converging with relative residual %0.5g', relres));
        break;
    end
    iter = iter + 1;
end
% Calculate final solution and residual.
if(isrprecond)
   x = x0 + M2 \ t;
else
   x = x0 + t;
end