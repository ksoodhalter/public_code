function [x, relres, resvec, iter, nmv] = gmres_kirk(A, b, tol, m, nrestarts, M1, M2, x0)
% [x, relres, resvec, iter] = gmres(A, b, tol, m, nrestarts, M1, M2, x0)
%
% A simple GMRES algorithm for the purposes of testing and teaching.
%
% DATA DICTIONARY
% INPUTS:
%   A  Coefficient matrix
%   b  The right-hand side.  Default is the vector of ones.
%   tol  A relative residual tolerance.  Default is 100.
%   m    The number of iterations per restart cycle or enter '[]' 
%        for no restarts.  Default is no restarts.
%   nrestarts   How many times to restart before the iteration ceases
%   M1   A possible left preconditioner or enter [] for none.  Default is
%        none.
%   M2   A possible right preconditioner of enter [] for none.  Default is
%        none.
%   x    A initial approximation.  Default is zero vector
% OUTPUTS:
%   x       The returned final approximation.
%   relres  The final relative residual for x.
%   resvec  A vector of absolute residual norms for each iteration.
%   iter    A 1x2 vector.  The first entry tells how many restart cycles
%           were were begun and the second entry tells us how many
%           iterations were completed in the last restart cycle before the
%           iteration stopped.  So iter = [20 5] means the code went
%           through 20 restart cycles, and in the last cycle, GMRES
%           stopped at the fifth iteration of that cycle
%   nmv     The total number of matrix-vector products executed.
%
% Copyright (c) 2013 Kirk M. Soodhalter
% kirk@math.soodhalter.com
% Version 2.01

%===============================================================================
% Copyright (c) 2013 Kirk Soodhalter
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction for non-commercial purposes, including
% without limitation the rights to use, copy, modify, merge, publish, and/or
% distribute copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in
% all copies or substantial portions of the Software, and credit has to be
% given to the authors in publications that are in any form based on this
% Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.
%===============================================================================

    n = size(A,1);

    if ~exist('b','var') || isempty(b)
        b = ones(n,1);
    end

    if ~exist('tol','var') || isempty(tol)
        tol = 1e-5;
    end

    if ~exist('m','var') || isempty(m)
        m = 100;
    end

    if ~exist('nrestarts','var') || isempty(nrestarts)
        nrestarts = 5;
    end

    % Check preconditioners
    existM1 = ~( ~exist('M1','var') || isempty(M1) );
    if ~(existM1)
       M1 = [];
    end
    existM2 = ~( ~exist('M2','var') || isempty(M2) );
    if ~(existM2)
       M2 = [];
    end

    if ~exist('x0','var') || isempty(x0)
        x0 = zeros(n,1);
    end
    
    r= b-A*x0;
    cycle = 0;

    % initialize solution vector
    t = zeros(size(x0));

    % initialize matvec count
    nmv = 1;

    % Calculate initial preconditioned residual
    r = b - A*x0;
    if(existM1)
       r = M1 \ r;
    end
    
    % Calculate initial preconditioned residual norm
    resvec = norm(r);

    % Precondition rhs if required
    if(existM1)
       bnorm = norm(M1 \ b);
    else
       bnorm = norm(b);
    end

    while cycle < nrestarts
        [t,r,p,resvec_inner] = gmres_cycle(A,t,r,m,M1,M2,tol*bnorm);   
       % Record residual norms and increment matvec count
       resvec = [resvec resvec_inner];
       nmv = nmv + p;
       if p < m
            % We converged.  Assign all variables and exit
            if(existM2)
                x = x0 + M2 \ t;
            else
                x = x0 + t;
            end           
            fprintf('Restarted GMRES Converged\n');
            relres = resvec(nmv) / bnorm;
            iter = [cycle p];    
            return
       end
    end
    %We did not converge but hit max iterations
    fprintf('Restarted GMRES reached maximum iterations without converging\n');
    if(existM2)
        x = x0 + M2 \ t;
    else
        x = x0 + t;
    end           
    relres = resvec(nmv) / bnorm;
    iter = [cycle p];   
    return
end

function [x,r,j,resvec] = gmres_cycle(A,x,r,m,M1,M2,tol)
%this code executes a cycle of restarted GMRES
%INPUTS:
%A,m, M1, M2, and tol are defined as in the main function
%r is the residual vector at the start of the cyecle
%x is the approximation at the start of the cycle
%OUTPUTS:
%x,r are the approximation and residual at the end of the cycle
%j is the number of iterations completed (<m if we converged this cycle)
%resvec is the vector of absolute residual norms for this cycle.

    if(nargin < 5 || isempty(M1))
       existM1 = 0;
    else
       existM1 = 1;
    end
    if(nargin < 6 || isempty(M2))
       existM2 = 0;
    else
       existM2 = 1;
    end

    % Initialize V
    V(:,1) = r / norm(r);

    % Initialize rhs for least-squares system
    rhs = zeros(m+1,1);
    rhs(1) = norm(r);

    % Do m steps of GMRES
    for j = 1:m
       % Preconditioned matvec
       if(existM2)
          V(:,j+1) = M2 \ V(:,j);
       else
          V(:,j+1) = V(:,j);
       end
       V(:,j+1) = A*V(:,j+1);
       if(existM1)
          V(:,j+1) = M1 \ V(:,j+1);
       end

       % Orthogonalize on V
       for i = 1:j
          H(i,j) = V(:,i)' * V(:,j+1);
          G(i,j) = H(i,j);
          V(:,j+1) = V(:,j+1) - H(i,j)*V(:,i);
       end
       H(j+1,j) = norm(V(:,j+1));
       G(j+1,j) = H(j+1,j);

       if (H(j+1,j) ~= 0.0)
          V(:,j+1) = V(:,j+1) / H(j+1,j);

          % Perform plane rotations on new column
          for i = 1:j-1,
             h1 = H(i,j);
             h2 = H(i+1,j);
             H(i,j)   = c(i) * h1 - s(i) * h2;
             H(i+1,j) = s(i) * h1 + c(i) * h2;
          end

          % Calculate new plane rotation      
          y1 = H(j,j);
          y2 = H(j+1,j);
          rh1 = rhs(j);
          rh2 = rhs(j+1);
          if y2 == 0.0
             c(j) = 1.0;
             s(j) = 0.0;
          elseif abs(y2) > abs(y1)
             h1 = -(y1/y2);
             s(j) = 1.0 / sqrt(1 + h1 * h1);
             c(j) = s(j) * h1;
          else
             h1 = -(y2/y1);
             c(j) = 1.0 / sqrt(1 + h1 * h1);
             s(j) = c(j) * h1;
          end
          h1       = y1;
          h2       = y2;
          H(j,j)   = c(j) * h1 - s(j) * h2;
          H(j+1,j) = s(j) * h1 + c(j) * h2;
          rhs(j)   = c(j) * rh1;      
          rhs(j+1) = s(j) * rh1;      

          resvec(j) = abs(rhs(j+1));

          if (resvec(j) < tol) 
             break;
          end

       end

    end

    % Solve least squares system
    y = H(1:j+1,1:j)\rhs(1:j+1);

    % Calculate solution
    x = x + V(:,1:j) * y;

    % Calculate residual
    x0 = zeros(j+1,1);
    x0(j+1) = -rhs(j+1);
    for i = j:-1:1
       x0(i)   = x0(i+1) * s(i);
       x0(i+1) = x0(i+1) * c(i);
    end
    x0(1) = x0(1) + norm(r);
    r = r - V(:,1:j+1)*x0;

end