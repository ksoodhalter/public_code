function krylov_orth_compare

steps = 50;

A = mmread('sherman5.mtx');
u =  mmread('sherman5_rhs1.mtx');

[~,Vgs] = arnoldi(A,u,steps,@gs_arnoldi_step);
[~,Vmgs] = arnoldi(A,u,steps,@mgs_arnoldi_step);
[Vhouse,~] = house_arnoldi(A,u,steps);

prodvec = orth_plot(Vgs,Vmgs,Vhouse);
close all;

fig = semilogy(prodvec(:,1),'k.-','linewidth',1,'markersize',15); hold on;
semilogy(prodvec(:,2),'r.-','linewidth',1,'markersize',15);
semilogy(prodvec(:,3),'b.-','linewidth',1,'markersize',15); hold off;


xlabel('Subspace Dimension ($j$)','interpreter','latex','fontsize',25);
ylabel('$\|I - V^{\ast}_{j}V_{j}\|$','interpreter','latex','fontsize',25);
%legend('Gram-Schmidt','Modified Gram-Schmidt','Householder');
set(gca,'fontsize',20)
ylim([1e-16 1e-9])  
%xlim([1 length(resvectrunc)])
set(gca,'xtick',[1 15:15:length(prodvec(:,2))])
set(gca,'ytick',10.^[-15:5:-5])
leg = legend('Gram-Schmidt','Modified Gram Schmidt','Householder');
set(leg,'Interpreter','Latex');
leg_pos = get(leg,'position') ;
set(leg,'position',[leg_pos(1),leg_pos(2),...
                      leg_pos(3)+4e-2,leg_pos(4)]) ;
title('Orthog. for 3 Arnoldi Algorithms','Interpreter','latex');
%saveas(fig, sprintf('%s%s',mfilename,'.eps'), 'psc2');
end

function [v,h] = mgs_arnoldi_step(A, V, trunc, M1, M2)

j = size(V,2);%what step our we on

if ~exist('trunc','var')
    trunc = j;
end

if ~exist('M1','var')
    M1 = [];
end

if ~exist('M2','var')
    M2 = [];
end

islprecond = ~isempty(M1);

isrprecond = ~isempty(M1);

h = zeros(j+1,1);

v = V(:,j);

%use right preconditioner
if isrprecond
    v = M2\v;
end

v = A*v;%push jth vector into next Krylov space

%use left preconditioner if available
if islprecond
    v = M1\v;
end

%orthogonalize
for k = max((j-trunc+1),1):j
    if floor(k) ~= k
        display('k is not an integer')
    end
    h(k) = dot(V(:,k),v);
    v = v - h(k)*V(:,k);
end

h(j+1) = norm(v);
v = v/h(j+1);

%reorthogonalize if we are doing full arnoldi and it is necessary
% if trunc == j && dot(V(:,1),v) > 1e-15
%     for k = max((j-trunc+1),1):j
%         v = v - dot(V(:,k),v)*V(:,k);
%     end
% end

end

function [V, H] = house_arnoldi(A, u, j)

n = size(A,2);
H = zeros(j+1,j);
V = zeros(n,j+1);
W = zeros(n,j+1);

V(:,1) = u/norm(u);

[V(:,1),W(:,1),foo] = house_arnoldi_step(A,u,[]);

for k=1:(j-1)
    [V(:,k+1),W(:,k+1),H(1:(k+1),k)] = house_arnoldi_step(A,V(:,k),W(:,1:k));
end

if j < n
    [V(:,j+1),W(:,j+1),H(1:(j+1),j)] = house_arnoldi_step(A,V(:,j),W(:,1:j));
else
    h = A*V(:,j);
    for k = 1:j%apply previous householder reflections
        h = house_project(h, W(:,k));
    end
    H(1:j,j) = h;
end

end

function [v,w,h] = house_arnoldi_step(A,v,W)

n = size(A,1);
v_new = zeros(n,1);
j = size(W,2);
v_new(j+1) = 1;%starts out as e_{j+1} the canonical basis vector

if j == 0%generating the first krylov vector
    h = v;
else%generating the j+1st Krylov vector
    h = A*v;%push into next krylov subspace

    for k = 1:j%apply previous householder reflections
        h = house_project(h, W(:,k));
    end
end

w = [zeros(j,1); house(h(j+1:n))];
h = house_project(h,w);

v_new = house_project(v_new,w);

for k = 1:j
    v_new = house_project(v_new,W(:,j-k+1));
end

v = v_new;
%v = v_new/norm(v_new);
h = h(1:j+1);

end

function [v,h] = gs_arnoldi_step(A, V, trunc, M1, M2)

j = size(V,2);%what step our we on

if ~exist('trunc','var')
    trunc = j;
end

if ~exist('M1','var')
    M1 = [];
end

if ~exist('M2','var')
    M2 = [];
end

islprecond = ~isempty(M1);

isrprecond = ~isempty(M1);

n = size(A,1);
j = size(V,2);%what step our we on

h = zeros(j+1,1);

v = V(:,j);

%use right preconditioner
if isrprecond
    v = M2\v;
end

v = A*v;%push jth vector into next Krylov space

%use left preconditioner if available
if islprecond
    v = M1\v;
end

w = zeros(n,1);%storing the projections of v.

%orthogonalize
for k=max((j-trunc+1),1):j
    h(k) = dot(V(:,k),v);
    w = w + h(k)*V(:,k);%store all the projections
end

v = v - w;%subtract projections
h(j+1) = norm(v);
v = v/h(j+1);

end

function [H,V] = arnoldi(A,w,j,method, trunc, M1,M2)


if ~exist('trunc','var') || isempty(trunc)
    trunc = j;
end

if ~exist('M1','var')
    M1 = [];
end

if ~exist('M2','var')
    M2 = [];
end

existM1 = ~isempty(M1);

n = size(A,1);
V = zeros(n,j);
H = zeros(j+1,j);

if existM1
    V(:,1) = M1\w;
else
    V(:,1) = w;
end
V(:,1) = V(:,1)/norm(V(:,1));

for k = 2:(j+1)
    [V(:,k),H(1:k,k-1)] = method(A,V(:,1:(k-1)),trunc,M1,M2);
end


end