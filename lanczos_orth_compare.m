function lanczos_orth_compare

steps = 50;

A = Laplace2D(25);
u =  onesmat(A);


[Vhouse,~] = house_arnoldi(A,u,steps);
[~,Vlanczos] = lanczos(A, u, steps);

prodvec = orth_plot(Vlanczos,Vhouse);
close all;

fig = semilogy(prodvec(:,1),'r.-','linewidth',1,'markersize',15); hold on;
semilogy(prodvec(:,2),'k.-','linewidth',1,'markersize',15); hold off;


xlabel('Subspace Dimension ($j$)','interpreter','latex','fontsize',25);
ylabel('$\|I - V^{\ast}_{j}V_{j}\|$','interpreter','latex','fontsize',25);
%legend('Gram-Schmidt','Modified Gram-Schmidt','Householder');
set(gca,'fontsize',20)
ylim([1e-16 1e-9])  
%xlim([1 length(resvectrunc)])
set(gca,'xtick',[1 15:15:length(prodvec(:,2))])
set(gca,'ytick',10.^[-15:5:-5])
leg = legend('Lanczos','Householder');
set(leg,'Interpreter','Latex');
leg_pos = get(leg,'position') ;
set(leg,'position',[leg_pos(1),leg_pos(2),...
                      leg_pos(3)+4e-2,leg_pos(4)]) ;
title('Orthog. of the Lanczos Vectors','Interpreter','latex');
saveas(fig, sprintf('%s%s',mfilename,'.eps'), 'psc2');

end

function [T,V] = lanczos(A, u, j)


n = size(A,2);
V = zeros(n,j);
T = sparse(j+1,j);

V(:,1) = u/norm(u);
v_old = zeros(n,1);
v_current = V(:,1);


[V(:,2), t_temp] = lanczos_step(A, [v_old v_current]);   
v_old = v_current;
v_current = V(:,2);
T(1:2,1)=t_temp(2:3);

for k = 3:j
    [V(:,k), T((k-2):(k),(k-1))] = lanczos_step(A, [v_old v_current]);   
    v_old = v_current;
    v_current = V(:,k);
end

end

function [v, t] = lanczos_step(A, V)


t = zeros(3,1);

w = A*V(:,2);

t(1) = dot(V(:,1),w);
w = w - t(1)*V(:,1);

t(2) = dot(V(:,2),w);
w = w - t(2)*V(:,2);

t(3) = norm(w);
v = w/t(3);

end

function [V, H] = house_arnoldi(A, u, j)


n = size(A,2);
H = zeros(j+1,j);
V = zeros(n,j+1);
W = zeros(n,j+1);

V(:,1) = u/norm(u);

[V(:,1),W(:,1),foo] = house_arnoldi_step(A,u,[]);

for k=1:(j-1)
    [V(:,k+1),W(:,k+1),H(1:(k+1),k)] = house_arnoldi_step(A,V(:,k),W(:,1:k));
end

if j < n
    [V(:,j+1),W(:,j+1),H(1:(j+1),j)] = house_arnoldi_step(A,V(:,j),W(:,1:j));
else
    h = A*V(:,j);
    for k = 1:j%apply previous householder reflections
        h = house_project(h, W(:,k));
    end
    H(1:j,j) = h;
end

end

function [v,w,h] = house_arnoldi_step(A,v,W)


n = size(A,1);
v_new = zeros(n,1);
j = size(W,2);
v_new(j+1) = 1;%starts out as e_{j+1} the canonical basis vector

if j == 0%generating the first krylov vector
    h = v;
else%generating the j+1st Krylov vector
    h = A*v;%push into next krylov subspace

    for k = 1:j%apply previous householder reflections
        h = house_project(h, W(:,k));
    end
end

w = [zeros(j,1); house(h(j+1:n))];
h = house_project(h,w);

v_new = house_project(v_new,w);

for k = 1:j
    v_new = house_project(v_new,W(:,j-k+1));
end

v = v_new;
v = v_new/norm(v_new);
h = h(1:j+1);

end

function L = Laplace2D(n)

I = speye(n,n);
E = sparse(2:n,1:n-1,1,n,n);
D = E+E'-2*I;
L = kron(D,I)*n^2+kron(I,D)*n^2;

end