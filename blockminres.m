function [X,relres,resvec,iter] = blockminres(A,B,tol,maxit,X0,P,residIndex,nRandBasis, QRMode, depTol)
% [x,relres,resvec,iter] = blockminres(A,B)
% [x,relres,resvec,iter] = blockminres(A,B,tol)
% [x,relres,resvec,iter] = blockminres(A,B,tol,maxit)
% [x,relres,resvec,iter] = blockminres(A,B,tol,maxit,X0)
% [x,relres,resvec,iter] = blockminres(A,B,tol,maxit,X0,M)
% [x,relres,resvec,iter] = blockminres(A,B,tol,maxit,X0,M, residIndex)
% [x,relres,resvec,iter] = blockminres(A,B,tol,maxit,X0,M, residIndex, nRandBasis)
% [x,relres,resvec,iter] = blockminres(A,B,tol,maxit,X0,M, residIndex, nRandBasis, QRMode)
% [x,relres,resvec,iter] = blockminres(A,B,tol,maxit,X0,M, residIndex, nRandBasis, QRMode, depTol)
%
% 
% 
% DATA DICTIONARY
% INPUTS:
% A            This is a square matrix of size N
% B            The right hand side of AX=B, it is an Nxp block vector
% tol          The relative residual tolerance
% maxit        Defines a maximum number of iterations to run 
%              before exiting regardless of reaching tolerance
% X0           An intial guess block solution
% M            The cholesky factor of a preconditioner M'*M.
% residIndex   Indices of the residuals we are actually interested in.  It
%              is a vector of logicals.  If entry i is TRUE then we 
%              check the norm of residual i when determining convergence
% nRandBasis   The number of replacement random basis vectors to construct 
%              ahead of time in case of basis dependence.
% QRMode       How to compute QR factorization of the upper hessenberg. 
%              Valid values are 'givens' and 'house'
% depTol       norm tolerance for detecting dependence of basis vector
% 
% OUTPUTS:
% X            The approximation being returned
% relres       The returned relative residual
% resvec       A vector of all computed residuals
% iter         The number of iterations

% INTERMEDIATE VARIABLES: 
% p             number of right-hand sides
% n             dimension in which we are working
% r             current column of Hessenberg matrix, which will be
%               transformed via Givens rotations
% rind          Indexing variable meant to keep track of indices in r
% nrnonzeros    Number of nonzero entries in r at current iteration
% nrleadzeros   Number of leading zeros in r at current iteration
% nrtrailzeros  Number of trailing zeros in r at current iteration
% nsymentries   Number of entries of r available without computation due 
%               to symmetry
% ngivenscols   How many sets of old givens rotations have we
% nVcols        Number of columns in V at current iteration
% r1stsubdiag   Index of location of first nonzero subdiagonal in r
% H             Matrix storing older lowersubdiagonal entries of Hessenberg
%               matrix for later use due to symmetry
% V             Matrix storing Lanczos vectors.  Treated like a FIFO queue.
% M             Matrix storing search directions.  Treated like a FIFO
%               queue.
% SE1           Matrix onto which Givens rotations are applied in order to
%               later perform progressive update of approximation
% s,c           Givens rotation matrices
% truenorms     Vector holding true residual norms, computed only when we
%               detect possible convergence using recursive residual
% R             Block residual
% Vindices      Index set allowing us to treat V as a queue without
%               actually moving data.
% Mindices      The same thing except so that M is a queue.
% depVecs       Vectors removed due to near-dependence. 
% depVecIndices Indices of the depVecs structure, so it acts as a queue.
%               The second row contains an integer indicating how many
%               iterations more this vector is required for
%               orthogonalization.
% Vrep          Replacement vectors with orthogonality maintained
% Vrep_indices  Indices of the Vrep structure
%
% SUBFUNCTIONS
% givens(), givensapply(), antidiag()
%
% Copyright (c) 2013 Kirk M. Soodhalter
% Version 2.0 with householder, block matvecs, near-dependent vectors
%                                              retained for orthogonalization
% Version 1.1 with subfunctions added.
% Associated to http://arxiv.org/abs/1301.2102

%===============================================================================
% Copyright (c) 2013 Kirk Soodhalter
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction for non-commercial purposes, including
% without limitation the rights to use, copy, modify, merge, publish, and/or
% distribute copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in
% all copies or substantial portions of the Software, and credit has to be
% given to the authors in publications that are in any form based on this
% Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.
%===============================================================================

N = size(A,1);
%some default settings

if ~exist('B','var')
    B = [ones(N,1) randn(N,1) rand(N,1) randn(N,1) eye(N,1)];
end

if ~exist('tol','var')
    tol = 1e-6;
end

if ~exist('maxit','var')
    maxit = 500;
end

if ~exist('X0','var')
    X0 = zeros(size(B));
end

if ~exist('P','var')
    P = [];
end

if ~exist('residIndex','var')
    residIndex = true(size(B,2),1);
end

if ~exist('nRandBasis','var') || isempty(nRandBasis)
    nRandBasis = 1;
end

if ~exist('QRMode','var')
    QRMode = 'givens';
else
    if ~strcmp(QRMode,'givens') && ~strcmp(QRMode,'house')
        error('Input value ''QRMode=%s'' not valid.\nThe only valid values are ''house'' and ''givens''.',QRMode);
    end
end

if ~exist('depTol', 'var')
    depTol = 1e-10;
end

isGivens = strcmp(QRMode,'givens');

isprecond = ~isempty(P);

if isprecond
    Pherm = P';
end

p = size(B,2);


resvec = zeros(maxit,p);

T = zeros(size(X0));
R = B - A*X0;

%We need the unpreconditioned initial residuals
%to test the relative residual for true convergence
BETA = zeros(1,p);
for i = 1:p
    BETA(i) = norm(R(:,i));
end

if isprecond
    R = (Pherm)\R;
end

for i = 1:p
    resvec(1,i) = norm(R(:,i));
end

relres = ones(1,p);
truenorms = zeros(1,p);
V = zeros(N,2*p);
M = zeros(N,2*p);
Vindices = 1:2*p;
Mindices = 1:2*p;
QRIndices = 1:2*p;
[V(:,Vindices(end-p+1:end)),S] = qr(R,0);
SE1 = [S;zeros(maxit,p)];
H = zeros(p,p);
s = zeros(p,2*p);
c = zeros(p,2*p);
house = zeros(p+1,2*p);
depVecs = zeros(N,p);
depVecIndices = [1:p; zeros(1,p)];
isNoMoreVecRepsMsg = true;

if nRandBasis > 0 %random vectors to be used as replacements in case 
                  %of basis dependence
    Vrep = rand(N,nRandBasis);
    Vrep = Vrep - V(:,Vindices(end-p+1:end))*(V(:,Vindices(end-p+1:end))'*Vrep);
    Vrep_indices = 1:nRandBasis;
end

if max(relres) <= tol
    display('Initial guess has residual below tolerance.');
    return;
end
iter = 1;


while true
    if mod(iter,p) == 0
        if mod(iter,11) == 0
            pnewline = '\n';
        else
            pnewline = '';
        end
        fprintf(sprintf('%s.',pnewline));
    end
    r = zeros(3*p+1,1);
    nrnonzeros = min(2*p+1,p+iter);
    nrleadzeros = min(p,max(0,iter-p-1));
    nrtrailzeros = 3*p+1-nrnonzeros-nrleadzeros;
    nsymentries = min(p,iter-1);
    nVcols = min(2*p,p+iter-1);
    nrotreflcols = min(2*p,iter-1);
    nsearchdir = min(2*p,iter-1);
    r1stsubdiag = nrleadzeros+nsymentries+2;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Compute New Arnoldi Vector          %
    % and Orthonganolize Against Previous %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if mod(iter,p) == 1 || p == 1
        %W = V(:,Vindices(p+1));
        W = V(:,Vindices(p+1:2*p));
        if isprecond
            W = P \ W;
        end
        W = A*W;
        if isprecond
            W = (Pherm)\W;
        end
    end
    colIdx = mod(iter,p); if colIdx == 0; colIdx = p; end
    
    if iter > 1
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Use Symmetry to Get Some       %
        % Orthogonalization Coefficients %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        r(nrleadzeros+nsymentries:-1:nrleadzeros+1) = antidiag(H(1:nsymentries,end-nsymentries+1:end));
        W(:, colIdx) = W(:, colIdx) - V(:,Vindices(end-nVcols+1:end-nVcols+nsymentries))*r(nrleadzeros+1:nrleadzeros+nsymentries);
    end
    
    for i=nVcols-1-nsymentries:-1:0
        rind = nVcols-i+nrleadzeros;
        r(rind) = V(:,Vindices(end-i))'*W(:, colIdx);
        W(:, colIdx) = W(:, colIdx) - r(rind)*V(:,Vindices(end-i));
    end
    r(nrleadzeros+nrnonzeros) = norm(W(:, colIdx));
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Linear dependence detection %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if r(nrleadzeros+nrnonzeros) >= depTol || nRandBasis==0
        if r(nrleadzeros+nrnonzeros) < depTol && isNoMoreVecRepsMsg
            isNoMoreVecRepsMsg = false;
            fprintf('\nLinear Dependence Detected, but no random basis ');
            fprintf('vectors available to replace dependent vector\n');
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Orthogonalize against       %
        % any saved dependent vectors %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if sum(depVecIndices(2,:)) > 0
            activeIdx = depVecIndices(1,depVecIndices(2,:)~=0); %indices for vectors still in usage
            W(:, colIdx) = W(:, colIdx) - depVecs(:,activeIdx) * (depVecs(:,activeIdx)'*W(:, colIdx));
            depVecIndices(2,depVecIndices(2,:)~=0) = depVecIndices(2,depVecIndices(2,:)~=0) - 1;
        end
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%
        % Normal banded Lanczos %
        %%%%%%%%%%%%%%%%%%%%%%%%%
        Vindices = [Vindices(2:end) Vindices(1)];
        V(:,Vindices(end)) = W(:, colIdx)/r(nrleadzeros+nrnonzeros);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Orthogonalize replacement vectors %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        Vrep = Vrep - V(:,Vindices(end))*(V(:,Vindices(end))'*Vrep);
    else
        fprintf('\nLinear Dependence Detected\n');
        r(nrleadzeros+nrnonzeros) = 0; %assign norm to zero
        
        depVecIndices = [depVecIndices(:,2:end) depVecIndices(:,1)]; %permute dependence vector indices
        depVecs(:,depVecIndices(1,end)) = W(:, colIdx)/norm(W(:, colIdx)); %save the nearly dependent vector for future orthog.
        depVecIndices(2,end) = 2*p; %newest vector will be used for orthog. for the next 2p iterations
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Still orthog replacements %
        % against new vector        %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        Vrep = Vrep - depVecs(:,depVecIndices(1,end))*(depVecs(:,depVecIndices(1,end))'*Vrep);
        depVecIndices(2,depVecIndices(2,:)~=0) = depVecIndices(2,depVecIndices(2,:)~=0) - 1; 
        depVecIndices(2,end) = depVecIndices(2,end) + 1; %this should not have been decreased, so iterate up
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Replace dependent vector %
        % with orthog. rand. one   %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        W(:, colIdx) = Vrep(:,Vrep_indices(end));
        Vrep_indices = Vrep_indices(1:end-1);
        nRandBasis = nRandBasis-1;
        Vindices = [Vindices(2:end) Vindices(1)];
        V(:,Vindices(end)) = W(:, colIdx)/norm(W(:, colIdx));
        Vrep = Vrep - V(:,Vindices(end))*(V(:,Vindices(end))'*Vrep);
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Save Lower Subdiag Entries      %
    % To Use For Orthog in Next Steps %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    H = [H(:,2:end) r(nrleadzeros+nsymentries + 2:end-nrtrailzeros)];
    
    if isGivens
        %%%%%%%%%%%%%%%%%%%%
        % Apply Previous   %
        % Givens Rotations %
        %%%%%%%%%%%%%%%%%%%%
        if iter > 1
           rind = 1;
           for i = nrotreflcols-1:-1:0
               for k = p:-1:1
                   [r(rind+k-1),r(rind+k)] = givensapply(s(k,end-i),c(k,end-i),r(rind+k-1),r(rind+k));
               end
               rind = rind + 1;
           end
        end
    else
        %%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Apply Previous          %
        % Householder Reflections %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%
        if iter > 1
            rind = 1;
            for i = nrotreflcols-1:-1:0
                r(rind:rind+p) = houseapply(house(:,QRIndices(end-i)),r(rind:rind+p));
                rind = rind + 1;
            end
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Apply Previous              %
    % Householder Transformations %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if isGivens
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Make Room for and Compute %
        % New Givens Rotations      %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        s = [s(:,2:end) zeros(p,1)];
        c = [c(:,2:end) zeros(p,1)];

        for i = p:-1:1
            rind = r1stsubdiag+i-2;
            [s(i,end),c(i,end)] = givens(r(rind), r(rind+1));
            [r(rind), ~] = givensapply(s(i,end),c(i,end),r(rind), r(rind+1));
            [SE1(iter+i-1,:),SE1(iter+i,:)] = givensapply(s(i,end),c(i,end),SE1(iter+i-1,:),SE1(iter+i,:));
        end
    else
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Compute and Store          %
        % New Householder Reflection %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        QRIndices = [QRIndices(2:end) QRIndices(1)];
        [house(:,QRIndices(end)),alpha] = housecomp(r(r1stsubdiag-1:r1stsubdiag+p-1));
        r(r1stsubdiag-1) = alpha;
        SE1(iter:iter+p,:) = houseapply(house(:,QRIndices(end)),SE1(iter:iter+p,:));
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Compute Newest Search Direction %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if iter == 1
        W(:, colIdx) = V(:,Vindices(p));
    else
        W(:, colIdx) = V(:,Vindices(p)) - M(:,Mindices(end-nsearchdir+1:end))*r(1:nsearchdir);
    end
    Mindices = [Mindices(2:end) Mindices(1)];
    M(:,Mindices(end)) = W(:, colIdx)/r(nsearchdir+1);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Compute New Approximation %
    % and Residual              %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    T = T + M(:,Mindices(end))*SE1(iter,:);
    if isGivens
        for i = 1:p
            resvec(iter+1,i) = norm(SE1(iter+1:iter+i,i));
        end
    else
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % SE1 is not upper triangular %
        % after application of        %
        % Householder reflections     %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        for i = 1:p
            resvec(iter+1,i) = norm(SE1(iter+1:iter+p,i));
        end
    end
    relres = resvec(iter+1,:)./resvec(1,:);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Check if Residual Tolerance Met %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if max(relres(residIndex)) <= tol
        %%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Make Sure True Residual %
        % Is Below Tolerance      %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%
        if isprecond
            X = X0 + P\T;
        else 
            X = X0 + T;
        end
        R = B-A*X;
        for i = 1:p
            truenorms(i) = norm(R(:,i));
        end
        maxnorm = max(truenorms(residIndex)./BETA(residIndex));
        if maxnorm <= tol
            display(sprintf('\n %s Converged at step %d. \n Max (columnwise) relative residual is %0.5g',mfilename,iter,maxnorm));
            break; 
        else
            fprintf(sprintf(' Computed max resid is %0.5g but true max resid is %0.5g\n', max(relres(residIndex)),maxnorm));
        end
    else
        if mod(iter,151) == 0
            fprintf(sprintf(' Computed max resid is %0.5g\n',max(relres(residIndex))));
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Check if Max Iterations Completed %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if iter == (maxit)
        if isprecond
            X = X0 + P\T;
        else 
            X = X0 + T;
        end
        R = B-A*X;
        for i = 1:p
            truenorms(i) = norm(R(:,i));
        end
        display(sprintf('\n %s reached maximum iterations without converging. \n Max (columnwise) relative residual %0.5g',mfilename, max(truenorms(residIndex)./BETA(residIndex))));
        break;
    end
    iter = iter + 1;
end%end while true drives each iteration

%%%%%%%%%%%%%%%%%%%%%
% Drop Extra Resvec %
%%%%%%%%%%%%%%%%%%%%%
resvec = resvec(1:iter+1,:);
if isprecond
    X = X0 + P\T;
else 
    X = X0 + T;
end

end %end of blockminres() main function

%%%%%%%%%%%%%%%%
% SUBFUNCTIONS %
%%%%%%%%%%%%%%%%

function [u, v] = givensapply(s, c, x, y)
% function [x, y] = givensapply(s, c, x, y)
%
% Applies the Givens rotation [c s; s -c] to the vector [x;y] where x and y
% can be scalars or row vectors of the same length.


u = c*x+s*y;
v = s*x - c*y;

end

function [s,c] = givens(x,y)
% [s,c] = givens(x,y)
%
% Computes the sine and cosine of a Givens rotation. 
%

if y==0
    s = 0;
    c = 1;
elseif x==0
    c = 0;
    s = 1;
else
    denom = sqrt(abs(x)^2+abs(y)^2);
    s = y/denom;
    c = x/denom;
end

end

function d = antidiag(A)
% function d = antidiag(A)%
%
% Retrieves the antidiagonal of a matrix as a vector. 
%
% Example:  if A=[a b; c d] then antidiag(A) = [b c]'
%

d = diag(fliplr(A));

end

function [reflvec, alpha] = housecomp(vect) 
    %
    % Computes the numerically stable Householder reflector
    %
    alpha = -sign(vect(1))*norm(vect);
    reflvec = vect - [alpha zeros(1,length(vect)-1)]';
    reflvec = reflvec/norm(reflvec);
end

function reflvec = houseapply(refl,targetvec)
    %
    % Applys the Householder reflector refl to targetvec
    %
    reflvec = targetvec - 2*refl*(refl'*targetvec);
end