function gmresCompare

A = mmread('sherman5.mtx');
b = mmread('sherman5_rhs1.mtx');
[L,U] = ilu(A);
[~,~,~,~,resvecfull] = gmres(A,b,100,1e-8,100,L,U,zerosmat(A));
[~,~,~,~,resvecrestart] = gmres(A,b,20,1e-8,100,L,U,zerosmat(A));
fig = semilogy(resvecfull/resvecfull(1),'k-','linewidth',2); hold on;
semilogy(resvecrestart/resvecrestart(1),'r--.','markersize',8,'linewidth',2); hold off;
set(gca,'fontsize',18)
ylim([1e-10 1e2]) 
%xlim([1 length(resvectrunc)])
set(gca,'xtick',[1 20:20:length(resvecrestart)])
set(gca,'ytick',10.^[-10:5:5])
legend('GMRES','GMRES(20)');
xlabel('Iterations','interpreter','latex');
ylabel('$\left\|r_k\right\|/\left\|r_0\right\|$','interpreter','latex');
%saveas(fig, sprintf('%s%s',mfilename,'.eps'), 'psc2');