function fomCompare
% Compares the performance of FOM with the truncated FOM (DIOM)
% for the sherman5 matrix found both in the Florida Sparse Matrix Library:
% http://www.cise.ufl.edu/research/sparse/matrices/
% This script also saves an .eps figure of the residual norms under the
% same name as the script.
%
% The requires Sherman5 matrix can be downloaded from:
% http://www.cise.ufl.edu/research/sparse/matrices/HB/sherman5.html
% 
% the trucation parameter is set to 20 by default but can be changed below.
trunc = 20;

A = mmread('sherman5.mtx');
[L,U] = ilu(A);
[xfull,relresfull,resvecfull] = diom(A,onesmat(A),1e-8,1e3,zerosmat(A),1e3,L,U);
[xtrunc,relrestrunc,resvectrunc] = diom(A,onesmat(A),1e-8,1e3,zerosmat(A),trunc,L,U);
fig = semilogy(resvecfull/resvecfull(1),'k-','linewidth',2); hold on;
semilogy(resvectrunc/resvectrunc(1),'r--.','markersize',8,'linewidth',2); hold off;
set(gca,'fontsize',18)
ylim([1e-10 1e2]) 
%xlim([1 length(resvectrunc)])
set(gca,'xtick',[1 20:20:length(resvectrunc)])
set(gca,'ytick',10.^[-10:5:5])
legend('FOM',sprintf('DIOM(%d)',trunc));
xlabel('Iterations','interpreter','latex');
ylabel('$\left\|r_k\right\|/\left\|r_0\right\|$','interpreter','latex');
saveas(fig, sprintf('%s%s',mfilename,'.eps'), 'psc2');