function [x, resvec, resids] = cgBasic(A, b, x, j)
% [x, resvec, resids] = cgBasic(A, b, x, j)
%
% CG coded just to use for some teaching applications
%
% DATA DICTIONARY
% INPUTS:
%   A   SPD coefficient matrix
%   b   Right hand side
%   x0  Initial approximation
%   j   How many iterations to perform
% OUTPUTS:
%   x         The cg approximation
%   resvec    residual norms
%   resids    Actual residual vectors for each itertion
%
% FUNCTIONAL DEPENDANCIES
%
% Copyright (c) 2013 Kirk M. Soodhalter
% 30-Apr-2013 14:39:08
% kirk@math.soodhalter.com

%===============================================================================
% Copyright (c) 2013 Kirk Soodhalter
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction for non-commercial purposes, including
% without limitation the rights to use, copy, modify, merge, publish, and/or
% distribute copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in
% all copies or substantial portions of the Software, and credit has to be
% given to the authors in publications that are in any form based on this
% Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.
%===============================================================================

r = b-A*x;
p = r;
resids{1} = r;
resvec(1) = norm(r);

for J=1:j
   rTr = r'*r;
   Ap = A*p;
   pTAp = p'*Ap;
   alpha = rTr/pTAp;
   x = x + alpha*p;
   r = r - alpha*Ap;
   resids{J+1} = r;
   resvec(J+1) = norm(r);
   rTr_old = rTr;
   rTr = r'*r;
   beta = rTr/rTr_old;
   p = r + beta*p;
end