function [X, out]  =  blockminres2(A, B, opts )
%  X  =  blockminres2(A, B) CALL WITH OPERATOR/RHS ONLY, ALL OPTIONS DEFAULT
%  [X, out]  =  blockminres2(A, B, opts) CALL WITH OPTIONS STRUCTURE TO ADJUST SOLVER BEHAVIOR
%
% An implementation of the block MINRES algorithm which has been built on
% top of the band Lanczos algorithm but with certain block operations still
% executed in block for efficiency, every p steps where p is the block
% size.
%
% FUNCTION INFORMATIONS
% INPUTS:
%    A       This is a square symmetric indefinite matrix of size NxN. It
%            can be a procedure or a matrix.  The code DOES NOT check if
%            the A is symmetric or indefinite.
%    B       The right hand side of AX=B, it is an Nxp block vector
%    opts    Structure containing optional inputs or options
%    -opts.tol         The relative residual tolerance: default - 1e-8
%    -opts.maxit       Defines a maximum number of iterations to run 
%                      before exiting regardless of reaching tolerance:
%                      default - 50
%    -opts.X0          An intial guess block solution: default - zeros(N,p)
%    -opts.P           The cholesky factor of a preconditioner P'*P or a 
%                      structure containing procedures applying inv(P) and inv(P') : default - []
%                      The code DOES NOT check if the preconditioner is
%                      appropriate.
%                      JUST TO BE CLEAR: one can set opts.P = MATRIX which
%                      is a Cholesky factor or opts.P = {PROCEDURE1,PROCEDURE2}
%                      where PROCEDURE1 applies the inverse of P and
%                      PROCEDURE2 applies the inverse of P'
%
%    -opts.residIndex  Indices of the residuals we are actually interested in.  It
%                      is a vector of logicals.  If entry i is TRUE then we 
%                      check the norm of residual i when determining
%                      convergence: default - true(1,p)
%    -opts.nRandBasis  The number of replacement random basis vectors to construct 
%                      ahead of time in case of basis dependence: 
%                      default - 2
%    -opts.depTol      Norm tolerance of unnormalized Lanczos vector to
%                      detect linear dependence: default - 1e-10
%    -opts.stopNorm    Do we stop based on block Frobenius norm or maximum
%                      column 2-norm: either 'fro' (default) or '2norm' 
%    -opts.QRMode      How to compute QR factorization of the upper hessenberg. 
%                      Valid values are 'givens' and 'house': 
%                      default - 'givens'
% OUTPUTS:
%    X      The approximation being returned
%    out    Structure containing optional outputs
%    -out.relres2norm  The returned relative residuals
%    -out.relres2fro   The returned relative residuals
%    -out.resvec2norm  A vector of all computed 2-norm residuals
%    -out.resvecFro    A vector of all computed Frobenius residuals
%    -out.iter         Number of iterations performed
% SUBFUNCTIONS
%    antidiag          Computes antidiagonal of a matrix
%    givens            Computes givens sine and cosine to annihilate the
%                      y-entry of a given vector
%    givensapply       Applys the givens rotation to a vector given a sine
%                      and cosine
%    housecomp         Given a vector, computes the stable householder
%                      reflector for that vector
%    houseappy         Given a householder reflector and a vector, computes
%                      the action of the reflector on that vector
%    col2norms         For a block of columns, computes the 2-norm of each
%                      column
%
% Kirk M. Soodhalter
% Version 1.1 subfunctions added
% Version 2.0 with householder, block matvecs, near-dependent vectors
%                  retained for orthogonalization
% Version 2.1 reduced inputs/outputs with options structure and defaults
% kirk@math.soodhalter.com
% 
% If you use this code, please cite the following paper:
% Kirk M. Soodhalter. 
% A block MINRES algorithm based on the banded Lanczos method. 
% Numerical Algorithms v. 69 n. 3 (2015), pp. 473-494

%===============================================================================
% Copyright (c) 2013 Kirk Soodhalter
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction for non-commercial purposes, including
% without limitation the rights to use, copy, modify, merge, publish, and/or
% distribute copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in
% all copies or substantial portions of the Software, and credit has to be
% given to the authors in publications that are in any form based on this
% Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.
%===============================================================================

    [N,p] = size(B);
    
    isAProc = isa(A,'function_handle');

    if ~exist('opts','var')
        opts = struct(); %empty structure
    end

    if isfield(opts,'tol')
        tol = opts.tol;
    else
        tol = 1e-8;
    end

    if isfield(opts,'depTol')
        depTol = opts.deptol;
    else
        depTol = 1e-10;
    end

    if isfield(opts,'X0')
        X0 = opts.X0;
    else
        X0 = zeros(N,p);
    end

    if isfield(opts,'P')
        isPrecond = true;
        if isstruct(P)
            if length(P) ~= 2 || ~isa(opts.P{1},'function_handle') || ~isa(opts.P{1},'function_handle')
                error('if opt.M is a structure, it must be length 2 and contain two function handles');
            else
                Pinv = opts.P{1};
                Ptransinv = opts.P{2};
            end
        elseif ismatrix(opts.P)
            Pinv = @(x) opts.P\x;
            Ptransinv = opts.P'\x;
        else
            error('%s%s%s','opt.P can only be a matrix representing the Cholesky factor ',...
                           'of an appropriate preconditioner or a structure {P,Ptrans} containing procedures ',...
                           'applying inv(P) and inv(P''), respectively.');
        end
    else
        P = [];
        isPrecond = false;
    end

    if isfield(opts,'maxit')
        maxit = opts.maxit;
    else
        maxit = 50;
    end

    if isfield(opts,'stopNorm')
        stopNorm = opts.stopNorm;
    else
        stopNorm = 'fro';
    end

    if isfield(opts,'QRMode')
        QRMode = opts.QRMode;
    else
        QRMode = 'fro';
    end
    
    isGivens = strcmp(QRMode,'givens');

    if isfield(opts,'resIndex')
        resIndex = opts.resIndex;
    else
        resIndex = true(1,p);
    end

    if isfield(opts,'nRandBasis')
        nRandBasis = opts.nRandBasis;
    else
        nRandBasis = 2;
    end

    if isfield(opts,'isOutNMV')
        isOutNMV = opts.isOutNMV;
    else
        isOutNMV = true;
    end

    if isfield(opts,'isOutResvecFro')
        isOutResvecFro = opts.isOutResvecFro;
    else
        isOutResvecFro = true;
    end

    if isfield(opts,'isOutResvec2norm')
        isOutResvec2norm = opts.isOutResvec2norm;
    else
        isOutResvec2norm = true;
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Logical array with true/false values       %
    % for various possible opt outputs           %    
    % so we can test if we need to initialize    %
    % opt                                        %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    isOpt = true(0);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Check to see if we                 %
    % should output the updated U        %
    % and the option is turned off       %
    % for all subsequent recursive calls %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if isfield(opts,'isOutNMV')
        isOutNMV = opts.isOutNMV;
    else
        isOutNMV = true;
    end
    isOpt = [isOpt isOutNMV];
    
    if isfield(opts,'isOutResvecFro')
        isOutResvecFro = opts.isOutResvecFro;
    else
        isOutResvecFro = true;
        %resvecs = zeros(maxcycles*m,s);
    end
    isOpt = [isOpt isOutResvecFro];
    
    if isfield(opts,'isOutResvec2norm')
        isOutResvec2norm = opts.isOutResvec2norm;
    else
        isOutResvec2norm = true;
        %resvecs = zeros(maxcycles*m,s);
    end
    isOpt = [isOpt isOutResvec2norm];

    if sum(isOpt) > 0
        out = struct();
        %put initializations here
    else
        out = [];
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Construct anon. function %
    % to apply base operator   %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if isAProc
        Amv = @(x) A(x);
    else
        Amv = @(x) A*x;
    end

    resvec2norm = zeros(maxit,p);
    resvecFro = zeros(maxit,1);

    T = zeros(size(X0));
    R = B - Amv(X0);

    %We need the unpreconditioned initial residuals
    %to test the relative residual for true convergence
    BETA = zeros(1,p);
    for i = 1:p
        BETA(i) = norm(R(:,i));
    end

    if isPrecond
        R =  Pinv(R);
    end

    resvecFro(1) = norm(R(:,resIndex));
    for i = 1:p
        resvec2norm(1,i) = norm(R(:,i));
    end

    relres2norm = ones(1,p);
    relresFro = 1;
    V = zeros(N,2*p);
    M = zeros(N,2*p);
    Vindices = 1:2*p;
    Mindices = 1:2*p;
    QRIndices = 1:2*p;
    [V(:,Vindices(end-p+1:end)),S] = qr(R,0);
    SE1 = [S;zeros(maxit,p)];
    H = zeros(p,p);
    s = zeros(p,2*p);
    c = zeros(p,2*p);
    house = zeros(p+1,2*p);
    depVecs = zeros(N,p);
    depVecIndices = [1:p; zeros(1,p)];
    isNoMoreVecRepsMsg = true;

    if nRandBasis > 0 %random vectors to be used as replacements in case 
                      %of basis dependence
        Vrep = rand(N,nRandBasis);
        Vrep = Vrep - V(:,Vindices(end-p+1:end))*(V(:,Vindices(end-p+1:end))'*Vrep);
        Vrep_indices = 1:nRandBasis;
    end

    if strcmp(stopNorm,'2norm')
        if max(relres2norm(:,resindex)) <= tol
            display('Initial guess has residual below tolerance.');
            return;
        end
    else
        if max(relresFro <= tol)
            display('Initial guess has residual below tolerance.');
            return;
        end
    end
    iter = 1;


    while true
        if mod(iter,p) == 0
            if mod(iter,11) == 0
                pnewline = '\n';
            else
                pnewline = '';
            end
            fprintf(sprintf('%s.',pnewline));
        end
        r = zeros(3*p+1,1);
        nrnonzeros = min(2*p+1,p+iter);
        nrleadzeros = min(p,max(0,iter-p-1));
        nrtrailzeros = 3*p+1-nrnonzeros-nrleadzeros;
        nsymentries = min(p,iter-1);
        nVcols = min(2*p,p+iter-1);
        nrotreflcols = min(2*p,iter-1);
        nsearchdir = min(2*p,iter-1);
        r1stsubdiag = nrleadzeros+nsymentries+2;

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Compute New Arnoldi Vector          %
        % and Orthonganolize Against Previous %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if mod(iter,p) == 1 || p == 1
            %W = V(:,Vindices(p+1));
            W = V(:,Vindices(p+1:2*p));
            if isPrecond
                W = Pinv(W);
            end
            W = Amv(W);
            if isPrecond
                W = Ptransinv(W);
            end
        end
        colIdx = mod(iter,p); if colIdx == 0; colIdx = p; end

        if iter > 1
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Use Symmetry to Get Some       %
            % Orthogonalization Coefficients %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            r(nrleadzeros+nsymentries:-1:nrleadzeros+1) = antidiag(H(1:nsymentries,end-nsymentries+1:end));
            W(:, colIdx) = W(:, colIdx) - V(:,Vindices(end-nVcols+1:end-nVcols+nsymentries))*r(nrleadzeros+1:nrleadzeros+nsymentries);
        end

        for i=nVcols-1-nsymentries:-1:0
            rind = nVcols-i+nrleadzeros;
            r(rind) = V(:,Vindices(end-i))'*W(:, colIdx);
            W(:, colIdx) = W(:, colIdx) - r(rind)*V(:,Vindices(end-i));
        end
        r(nrleadzeros+nrnonzeros) = norm(W(:, colIdx));

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Linear dependence detection %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if r(nrleadzeros+nrnonzeros) >= depTol || nRandBasis==0
            if r(nrleadzeros+nrnonzeros) < depTol && isNoMoreVecRepsMsg
                isNoMoreVecRepsMsg = false;
                fprintf('\nLinear Dependence Detected, but no random basis ');
                fprintf('vectors available to replace dependent vector\n');
            end

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Orthogonalize against       %
            % any saved dependent vectors %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if sum(depVecIndices(2,:)) > 0
                activeIdx = depVecIndices(1,depVecIndices(2,:)~=0); %indices for vectors still in usage
                W(:, colIdx) = W(:, colIdx) - depVecs(:,activeIdx) * (depVecs(:,activeIdx)'*W(:, colIdx));
                depVecIndices(2,depVecIndices(2,:)~=0) = depVecIndices(2,depVecIndices(2,:)~=0) - 1;
            end


            %%%%%%%%%%%%%%%%%%%%%%%%%
            % Normal banded Lanczos %
            %%%%%%%%%%%%%%%%%%%%%%%%%
            Vindices = [Vindices(2:end) Vindices(1)];
            V(:,Vindices(end)) = W(:, colIdx)/r(nrleadzeros+nrnonzeros);

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Orthogonalize replacement vectors %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            Vrep = Vrep - V(:,Vindices(end))*(V(:,Vindices(end))'*Vrep);
        else
            fprintf('\nLinear Dependence Detected\n');
            r(nrleadzeros+nrnonzeros) = 0; %assign norm to zero

            depVecIndices = [depVecIndices(:,2:end) depVecIndices(:,1)]; %permute dependence vector indices
            depVecs(:,depVecIndices(1,end)) = W(:, colIdx)/norm(W(:, colIdx)); %save the nearly dependent vector for future orthog.
            depVecIndices(2,end) = 2*p; %newest vector will be used for orthog. for the next 2p iterations

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Still orthog replacements %
            % against new vector        %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            Vrep = Vrep - depVecs(:,depVecIndices(1,end))*(depVecs(:,depVecIndices(1,end))'*Vrep);
            depVecIndices(2,depVecIndices(2,:)~=0) = depVecIndices(2,depVecIndices(2,:)~=0) - 1; 
            depVecIndices(2,end) = depVecIndices(2,end) + 1; %this should not have been decreased, so iterate up


            %%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Replace dependent vector %
            % with orthog. rand. one   %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%
            W(:, colIdx) = Vrep(:,Vrep_indices(end));
            Vrep_indices = Vrep_indices(1:end-1);
            nRandBasis = nRandBasis-1;
            Vindices = [Vindices(2:end) Vindices(1)];
            V(:,Vindices(end)) = W(:, colIdx)/norm(W(:, colIdx));
            Vrep = Vrep - V(:,Vindices(end))*(V(:,Vindices(end))'*Vrep);
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Save Lower Subdiag Entries      %
        % To Use For Orthog in Next Steps %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        H = [H(:,2:end) r(nrleadzeros+nsymentries + 2:end-nrtrailzeros)];

        if isGivens
            %%%%%%%%%%%%%%%%%%%%
            % Apply Previous   %
            % Givens Rotations %
            %%%%%%%%%%%%%%%%%%%%
            if iter > 1
               rind = 1;
               for i = nrotreflcols-1:-1:0
                   for k = p:-1:1
                       [r(rind+k-1),r(rind+k)] = givensapply(s(k,end-i),c(k,end-i),r(rind+k-1),r(rind+k));
                   end
                   rind = rind + 1;
               end
            end
        else
            %%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Apply Previous          %
            % Householder Reflections %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%
            if iter > 1
                rind = 1;
                for i = nrotreflcols-1:-1:0
                    r(rind:rind+p) = houseapply(house(:,QRIndices(end-i)),r(rind:rind+p));
                    rind = rind + 1;
                end
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Apply Previous              %
        % Householder Transformations %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if isGivens
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Make Room for and Compute %
            % New Givens Rotations      %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            s = [s(:,2:end) zeros(p,1)];
            c = [c(:,2:end) zeros(p,1)];

            for i = p:-1:1
                rind = r1stsubdiag+i-2;
                [s(i,end),c(i,end)] = givens(r(rind), r(rind+1));
                [r(rind), ~] = givensapply(s(i,end),c(i,end),r(rind), r(rind+1));
                [SE1(iter+i-1,:),SE1(iter+i,:)] = givensapply(s(i,end),c(i,end),SE1(iter+i-1,:),SE1(iter+i,:));
            end
        else
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Compute and Store          %
            % New Householder Reflection %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            QRIndices = [QRIndices(2:end) QRIndices(1)];
            [house(:,QRIndices(end)),alpha] = housecomp(r(r1stsubdiag-1:r1stsubdiag+p-1));
            r(r1stsubdiag-1) = alpha;
            SE1(iter:iter+p,:) = houseapply(house(:,QRIndices(end)),SE1(iter:iter+p,:));
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Compute Newest Search Direction %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if iter == 1
            W(:, colIdx) = V(:,Vindices(p));
        else
            W(:, colIdx) = V(:,Vindices(p)) - M(:,Mindices(end-nsearchdir+1:end))*r(1:nsearchdir);
        end
        Mindices = [Mindices(2:end) Mindices(1)];
        M(:,Mindices(end)) = W(:, colIdx)/r(nsearchdir+1);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Compute New Approximation %
        % and Residual              %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        T = T + M(:,Mindices(end))*SE1(iter,:);
        if isGivens
            resvecFro(iter+1) = norm(SE1(iter+1:iter+p,resIndex));
            for i = 1:p
                resvec2norm(iter+1,i) = norm(SE1(iter+1:iter+i,i));
            end
        else
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % SE1 is not upper triangular %
            % after application of        %
            % Householder reflections     %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            resvecFro(iter+1) = norm(SE1(iter+1:iter+p,resIndex));
            for i = 1:p
                resvec2norm(iter+1,i) = norm(SE1(iter+1:iter+p,i));
            end
        end
        relres2norm = resvec2norm(iter+1,:)./resvec2norm(1,:);
        relresFro = resvecFro(iter+1)/resvecFro(1);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Check if Residual Tolerance Met %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        if (strcmp(stopNorm,'2norm') && max(relres2norm(residIndex)) <= tol) || (strcmp(stopNorm,'fro') && relresFro <= tol)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Make Sure True Residual %
            % Is Below Tolerance      %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%
            if isPrecond
                X = X0 + Pinv(T);
            else 
                X = X0 + T;
            end
            
            R = B - Amv(X);
            
            if strcmp(stopNorm,'2norm')
                testnorm = max(col2norms(R));
            else
                testnorm = norm(R);
            end
            
            if testnorm <= tol
                display(sprintf('\n %s Converged at step %d. \n Max (columnwise) relative residual is %0.5g',mfilename,iter,testnorm));
                break; 
            else
                fprintf(sprintf(' Computed max resid is %0.5g but true max resid is %0.5g\n', max(relres2norm(residIndex)),testnorm));
            end
        else
            if strcmp(stopNorm,'2norm')
                testnorm = max(relres2norm(resIndex));
            else
                testnorm = relresFro;
            end
            
            if mod(iter,151) == 0
                fprintf(sprintf(' Computed relative residual is %0.5g\n',testnorm));
            end
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Check if Max Iterations Completed %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if iter == (maxit)
            if isPrecond
                X = X0 + Pinv(T);
            else 
                X = X0 + T;
            end
            
            if strcmp(stopNorm,'2norm')
                testnorm = max(relres2norm(resIndex));
            else
                testnorm = relresFro;
            end
            
            display(sprintf('\n %s reached maximum iterations without converging. \n Relative residual %0.5g',mfilename, testnorm));
            break;
        end
        iter = iter + 1;
    end%end while true drives each iteration

    %%%%%%%%%%%%%%%%%%%%
    % Generate outputs %
    %%%%%%%%%%%%%%%%%%%%
    out.relres2norm = relres2norm;
    out.relresFro = relresFro;
    
    if isOutResvec2norm
        out.resvec2norm = resvec2norm(1:iter+1,:);
    end
    
    if isOutResvecFro
        out.resvecFro = resvecFro(1:iter+1);
    end
    
    if isOutNMV
        out.iter = iter;
    end

end %end of blockminres() main function

function [u,v] = givensapply(s, c, x, y)
    u = c*x+s*y;
    v = s*x - c*y;
end

function [s,c] = givens(x,y)
    if y==0
        s = 0;
        c = 1;
    elseif x==0
        c = 0;
        s = 1;
    else
        denom = sqrt(abs(x)^2+abs(y)^2);
        s = y/denom;
        c = x/denom;
    end
end

function d = antidiag(A)
    d = diag(fliplr(A));
end

function [reflvec, alpha] = housecomp(vect) 
    alpha = -sign(vect(1))*norm(vect);
    reflvec = vect - [alpha zeros(1,length(vect)-1)]';
    reflvec = reflvec/norm(reflvec);
end

function reflvec = houseapply(refl,targetvec)
    reflvec = targetvec - 2*refl*(refl'*targetvec);
end

function colnorms = col2norms(F)
    colnorms = sqrt(sum(F.^2));
end
